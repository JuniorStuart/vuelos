﻿using Aeropuerto.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aeropuerto
{
    public partial class Login : Form
    {
        private readonly UsuariosManager manager;
        public Login()
        {
            InitializeComponent();
            manager = new UsuariosManager();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (manager.ValidarUsuario(textBox1.Text, textBox2.Text))
            {
                var form = new Form1();
                form.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Usuario o contrasena incorrectos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
