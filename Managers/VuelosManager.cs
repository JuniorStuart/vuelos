﻿using Aeropuerto.Models;
using Aeropuerto.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aeropuerto.Managers
{
    public class VuelosManager
    {
        private readonly VuelosRepository repository;
        public VuelosManager()
        {
            repository = new VuelosRepository();
        }

        public Vuelos[] ObtenerVuelos()
        {

            return repository.ObtenerVuelos();

        }

        public void InsertarVuelo(Vuelos vuelo)
        {
            repository.InsertarVuelo(vuelo);
        }
        public void ActualizarVuelo(Vuelos vuelo)
        {
            repository.ActualizarVuelo(vuelo);
        }
        public void EliminarVuelo(int id)
        {
            repository.EliminarVuelo(id);
        }

    }
}
