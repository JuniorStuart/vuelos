﻿using Aeropuerto.Models;
using Aeropuerto.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aeropuerto.Managers
{
    public class UsuariosManager
    {
        private readonly UsuariosRepository repository;
        public UsuariosManager()
        {
            repository = new UsuariosRepository();
        }

        public bool ValidarUsuario(string usuario, string contrasena)
        {
            return repository.ValidarUsuario(usuario, contrasena);
        }
    }
}
