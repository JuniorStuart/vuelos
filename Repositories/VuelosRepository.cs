﻿using Aeropuerto.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aeropuerto.Repositories
{
    class VuelosRepository
    {
        private readonly string connectionString;
        public VuelosRepository()
        {
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["Vuelos"].ConnectionString;
        }

        public Vuelos[] ObtenerVuelos()
        {
            var vuelos = new List<Vuelos>();
            var conexion = new SqlConnection(connectionString);
            conexion.Open();
            var comand = new SqlCommand("SELECT *FROM Vuelos", conexion);
            var reader = comand.ExecuteReader();
            while (reader.Read())
            {
                vuelos.Add(new Vuelos()
                {
                    Id = Int32.Parse(reader[0].ToString()),
                    CiudadOrigen = (reader[1].ToString()),
                    CiudadDestino=(reader[2].ToString()),
                    Fecha = (reader[3]).ToString(),
                    HoraSalida= (reader[4]).ToString(),
                    HoraLlegada= (reader[5]).ToString(),
                    NumerodeVuelo= (reader[6]).ToString(),
                    Aerolinea=(reader[7]).ToString(),
                    EstadodeVuelo= (reader[8]).ToString(),
                });
            }
            conexion.Close();
            return vuelos.ToArray();
        }

        public void InsertarVuelo(Vuelos vuelo)
        {
            var conexion = new SqlConnection(connectionString);
            conexion.Open();
            var comand = new SqlCommand("Insert into Vuelos (CiudadOrigen,CiudadDestino,Fecha,HoraSalida,HoraLlegada,NumerodeVuelo,Aerolinea,EstadodeVuelo)values('" + vuelo.CiudadOrigen + "','" +vuelo.CiudadDestino + "','" + vuelo.Fecha + "','" + vuelo.HoraSalida+ "','" + vuelo.HoraLlegada+ "','" + vuelo.NumerodeVuelo + "','" + vuelo.Aerolinea + "','" +vuelo.EstadodeVuelo + "')", conexion);
            comand.ExecuteNonQuery();
            conexion.Close();
        }
        public void ActualizarVuelo(Vuelos vuelo)
        {

            var conexion = new SqlConnection(connectionString);
            conexion.Open();
            var comand = new SqlCommand("update  Vuelos  set CiudadOrigen='" + vuelo.CiudadOrigen + "',CiudadDestino='" + vuelo.CiudadDestino + "', Fecha='" + vuelo.Fecha + "', HoraSalida='" + vuelo.HoraSalida + "', HoraLlegada='" + vuelo.HoraLlegada + "', NumerodeVuelo='" + vuelo.NumerodeVuelo+ "', Aerolinea='" + vuelo.Aerolinea + "', EstadodeVuelo='" + vuelo.EstadodeVuelo + "' where id='" + vuelo.Id.ToString() + "'", conexion);
            comand.ExecuteNonQuery();
            conexion.Close();          

        }
        public void EliminarVuelo(int Id)
        {
            var conexion = new SqlConnection(connectionString);
            conexion.Open();
            var comand = new SqlCommand("Delete from Vuelos where id= '" + Id.ToString() +  "'", conexion);
            comand.ExecuteNonQuery();
            conexion.Close();
        }


    }
}
