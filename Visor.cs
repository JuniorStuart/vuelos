﻿using Aeropuerto.Managers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Aeropuerto
{
    public partial class Visor : Form
    {
        private readonly VuelosManager manager;
        public Visor()
        {
            InitializeComponent();
            manager = new VuelosManager();
        }

        private void Visor_Load(object sender, EventArgs e)
        {
            llenargrid();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void llenargrid()
        {
            var vuelos = manager.ObtenerVuelos();
            dataGridView1.DataSource = vuelos;
        }
    }
}
