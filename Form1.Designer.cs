﻿
namespace Aeropuerto
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.Add = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.Actualizar = new System.Windows.Forms.Button();
            this.CiudaddeOrigen = new System.Windows.Forms.TextBox();
            this.CiudaddeDestino = new System.Windows.Forms.TextBox();
            this.Fecha = new System.Windows.Forms.TextBox();
            this.HoraSalida = new System.Windows.Forms.TextBox();
            this.HoraLlegada = new System.Windows.Forms.TextBox();
            this.NumerodeVuelo = new System.Windows.Forms.TextBox();
            this.Aerolinea = new System.Windows.Forms.TextBox();
            this.EstadodeVuelo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.LlenarGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.LlenarGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(260, 33);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(75, 23);
            this.Add.TabIndex = 0;
            this.Add.Text = "AGREGAR";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Delete
            // 
            this.Delete.Location = new System.Drawing.Point(260, 143);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(75, 23);
            this.Delete.TabIndex = 1;
            this.Delete.Text = "ELIMINAR";
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Actualizar
            // 
            this.Actualizar.Location = new System.Drawing.Point(260, 270);
            this.Actualizar.Name = "Actualizar";
            this.Actualizar.Size = new System.Drawing.Size(87, 23);
            this.Actualizar.TabIndex = 2;
            this.Actualizar.Text = "ACTUALIZAR";
            this.Actualizar.UseVisualStyleBackColor = true;
            this.Actualizar.Click += new System.EventHandler(this.button3_Click);
            // 
            // CiudaddeOrigen
            // 
            this.CiudaddeOrigen.Location = new System.Drawing.Point(154, 33);
            this.CiudaddeOrigen.Name = "CiudaddeOrigen";
            this.CiudaddeOrigen.Size = new System.Drawing.Size(100, 20);
            this.CiudaddeOrigen.TabIndex = 3;
            this.CiudaddeOrigen.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // CiudaddeDestino
            // 
            this.CiudaddeDestino.Location = new System.Drawing.Point(154, 62);
            this.CiudaddeDestino.Name = "CiudaddeDestino";
            this.CiudaddeDestino.Size = new System.Drawing.Size(100, 20);
            this.CiudaddeDestino.TabIndex = 4;
            // 
            // Fecha
            // 
            this.Fecha.Location = new System.Drawing.Point(154, 94);
            this.Fecha.Name = "Fecha";
            this.Fecha.Size = new System.Drawing.Size(100, 20);
            this.Fecha.TabIndex = 5;
            // 
            // HoraSalida
            // 
            this.HoraSalida.Location = new System.Drawing.Point(154, 129);
            this.HoraSalida.Name = "HoraSalida";
            this.HoraSalida.Size = new System.Drawing.Size(100, 20);
            this.HoraSalida.TabIndex = 6;
            // 
            // HoraLlegada
            // 
            this.HoraLlegada.Location = new System.Drawing.Point(154, 165);
            this.HoraLlegada.Name = "HoraLlegada";
            this.HoraLlegada.Size = new System.Drawing.Size(100, 20);
            this.HoraLlegada.TabIndex = 7;
            // 
            // NumerodeVuelo
            // 
            this.NumerodeVuelo.Location = new System.Drawing.Point(154, 202);
            this.NumerodeVuelo.Name = "NumerodeVuelo";
            this.NumerodeVuelo.Size = new System.Drawing.Size(100, 20);
            this.NumerodeVuelo.TabIndex = 8;
            // 
            // Aerolinea
            // 
            this.Aerolinea.Location = new System.Drawing.Point(154, 241);
            this.Aerolinea.Name = "Aerolinea";
            this.Aerolinea.Size = new System.Drawing.Size(100, 20);
            this.Aerolinea.TabIndex = 9;
            // 
            // EstadodeVuelo
            // 
            this.EstadodeVuelo.Location = new System.Drawing.Point(154, 273);
            this.EstadodeVuelo.Name = "EstadodeVuelo";
            this.EstadodeVuelo.Size = new System.Drawing.Size(100, 20);
            this.EstadodeVuelo.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "CIUDAD DE ORIGEN";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(25, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "CIUDAD DE DESTINO";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(25, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "FECHA";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(25, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "HORA DE SALIDA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 166);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "HORA DE LLEGADA";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(25, 203);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(112, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "NUMERO DE VUELO";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(25, 242);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "AEROLINEA";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 274);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(108, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "ESTADO DE VUELO";
            // 
            // LlenarGrid
            // 
            this.LlenarGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.LlenarGrid.Location = new System.Drawing.Point(353, 33);
            this.LlenarGrid.Name = "LlenarGrid";
            this.LlenarGrid.Size = new System.Drawing.Size(435, 271);
            this.LlenarGrid.TabIndex = 19;
            this.LlenarGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.LlenarGrid.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.LlenarGrid_CellContentClick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LlenarGrid);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.EstadodeVuelo);
            this.Controls.Add(this.Aerolinea);
            this.Controls.Add(this.NumerodeVuelo);
            this.Controls.Add(this.HoraLlegada);
            this.Controls.Add(this.HoraSalida);
            this.Controls.Add(this.Fecha);
            this.Controls.Add(this.CiudaddeDestino);
            this.Controls.Add(this.CiudaddeOrigen);
            this.Controls.Add(this.Actualizar);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.Add);
            this.Name = "Form1";
            this.Text = "Administrador de vuelos";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.LlenarGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Actualizar;
        private System.Windows.Forms.TextBox CiudaddeOrigen;
        private System.Windows.Forms.TextBox CiudaddeDestino;
        private System.Windows.Forms.TextBox Fecha;
        private System.Windows.Forms.TextBox HoraSalida;
        private System.Windows.Forms.TextBox HoraLlegada;
        private System.Windows.Forms.TextBox NumerodeVuelo;
        private System.Windows.Forms.TextBox Aerolinea;
        private System.Windows.Forms.TextBox EstadodeVuelo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView LlenarGrid;
    }
}

