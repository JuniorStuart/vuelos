﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Aeropuerto.Models
{
    public class Vuelos
    {
        public int Id { get; set; }
        public string  CiudadOrigen { get; set; }
        public string CiudadDestino { get; set; }
        public string Fecha { get; set; }
        public string HoraSalida { get; set; }
        public string HoraLlegada { get; set; }
        public string NumerodeVuelo { get; set; }
        public string Aerolinea { get; set; }
        public string EstadodeVuelo { get; set; }
    }
}
