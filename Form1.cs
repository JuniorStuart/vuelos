﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using Aeropuerto.Managers;
using Aeropuerto.Models;

namespace Aeropuerto
{
    public partial class Form1 : Form
    {
        private readonly VuelosManager manager;
        public Form1()
        {
            InitializeComponent();
            manager = new VuelosManager();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Add_Click(object sender, EventArgs e)
        {
            var vuelo = new Vuelos()
            {
                CiudadOrigen = CiudaddeOrigen.Text, CiudadDestino = CiudaddeDestino.Text, Fecha = Fecha.Text, HoraSalida = HoraSalida.Text, HoraLlegada = HoraLlegada.Text, NumerodeVuelo = NumerodeVuelo.Text, Aerolinea = Aerolinea.Text, EstadodeVuelo = EstadodeVuelo.Text
            };
            manager.InsertarVuelo(vuelo);
            llenargrid();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            manager.EliminarVuelo(Int32.Parse(LlenarGrid.CurrentRow.Cells[0].Value.ToString()));
            llenargrid();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            CiudaddeOrigen.Text = LlenarGrid.Rows[e.RowIndex].Cells[1].Value.ToString();
            CiudaddeDestino.Text = LlenarGrid.Rows[e.RowIndex].Cells[2].Value.ToString();
            Fecha.Text = LlenarGrid.Rows[e.RowIndex].Cells[3].Value.ToString();
            HoraSalida.Text = LlenarGrid.Rows[e.RowIndex].Cells[4].Value.ToString();
            HoraLlegada.Text = LlenarGrid.Rows[e.RowIndex].Cells[5].Value.ToString();
            NumerodeVuelo.Text = LlenarGrid.Rows[e.RowIndex].Cells[6].Value.ToString();
            Aerolinea.Text = LlenarGrid.Rows[e.RowIndex].Cells[7].Value.ToString();
            EstadodeVuelo.Text = LlenarGrid.Rows[e.RowIndex].Cells[8].Value.ToString();           
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            llenargrid();
        }
        private void llenargrid()
        {
            var vuelos = manager.ObtenerVuelos();
            LlenarGrid.DataSource = vuelos;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var vuelo = new Vuelos()
            {
                Id = Int32.Parse(LlenarGrid.CurrentRow.Cells[0].Value.ToString()),
                CiudadOrigen = CiudaddeOrigen.Text,
                CiudadDestino = CiudaddeDestino.Text,
                Fecha = Fecha.Text,
                HoraSalida = HoraSalida.Text,
                HoraLlegada = HoraLlegada.Text,
                NumerodeVuelo = NumerodeVuelo.Text,
                Aerolinea = Aerolinea.Text,
                EstadodeVuelo = EstadodeVuelo.Text
            };

            manager.ActualizarVuelo(vuelo);
            MessageBox.Show("su registro ingreso exitosamente", "EXITO", MessageBoxButtons.OK, MessageBoxIcon.Information);
            llenargrid();
            CiudaddeOrigen.Text = "";
            CiudaddeDestino.Text = "";
            Fecha.Text = "";
            HoraSalida.Text = "";
            HoraLlegada.Text = "";
            NumerodeVuelo.Text = "";
            Aerolinea.Text = "";
            EstadodeVuelo.Text = "";


        }

        private void LlenarGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
